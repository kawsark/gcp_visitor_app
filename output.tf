output "server_addresses" {
  value = module.compute_instance.addresses
}
