//--------------------------------------------------------------------
// Variables
variable "compute_instance_name_prefix" {}
variable "network_subnet_name" {}
variable "network_gcp_network_name" {}

variable "network_firewall_rule_name" {
  default = "visitorapp-http"
}

variable "machine_type" {
  default = "n1-standard-2"
}

variable "network_description" {
  default = "visitorapp-network"
}

//--------------------------------------------------------------------
// Modules
module "compute_instance" {
  #source  = "app.terraform.io/GitlabCI-demos/compute-instance/google"
  source  = "github.com/kawsark/terraform-google-compute-instance"
  #version = "0.1.7"

  machine_type = var.machine_type
  name_prefix = "${var.compute_instance_name_prefix}"
  subnetwork = "${module.network_subnet.self_link}"
}

module "network_firewall" {
  source  = "app.terraform.io/GitlabCI-demos/network-firewall/google"
  version = "0.1.7"

  description = "Enable HTTP for visitorapp"
  name = var.network_firewall_rule_name
  network = "${module.network.network_self_link}"
  ports = ["80"]
  source_ranges = ["173.54.1.28/32"]
}

module "network_subnet" {
  source  = "app.terraform.io/GitlabCI-demos/network-subnet/google"
  version = "0.1.5"

  description = "a private subnet"
  name = "${var.network_subnet_name}"
  vpc = "${module.network.network_self_link}"
}

module "network" {
  source  = "app.terraform.io/GitlabCI-demos/network/google"
  version = "0.1.5"

  auto_create_subnetworks = "false"
  description = var.network_description
  gcp_network_name = "${var.network_gcp_network_name}"
}
