# gcp_visitor_app

Example top level module for PMR demo.
Branches:
1. master
   - Default branch to apply from
   - Workspace: gcp_visitor_app [master branch]
2. remote
   - CLI driven run using remote backend
   - Workspace: gcp_visitor_app-cli
3. dev
   - experimental changes
   - Workspace: gcp_visitor_app-dev [dev branch]

### VCS workflow
Login to TFE/C and set token
```
terraform login
export TFH_token=<api-token>
```

Set Workspace variables
We are using the [TF Helper tool](https://github.com/hashicorp-community/tf-helper) to set these variables.
```
export TFH_org=<org-name>
export TFH_name=gcp_visitor_app
tfh workspace list

tfh pushvars -senv-var GOOGLE_CREDENTIALS=${GOOGLE_CREDENTIALS}
tfh pushvars -env-var GOOGLE_PROJECT="${GOOGLE_PROJECT}"
tfh pushvars -env-var GOOGLE_REGION="us-east1"

tfh pushvars -var compute_instance_name_prefix="visitorapp" \
             -var network_subnet_name="visitorapp-network" \
             -var network_gcp_network_name="visitorapp-network"
```

Queue a Run by merging or via the UI/API