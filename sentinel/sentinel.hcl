policy "restrict-gce-machine-type" {
    enforcement_level = "soft-mandatory"
}

policy "require-all-resources-from-pmr" {
    enforcement_level = "hard-mandatory"
}